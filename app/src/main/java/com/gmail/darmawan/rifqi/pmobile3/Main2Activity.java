package com.gmail.darmawan.rifqi.pmobile3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Arrays;

public class Main2Activity extends AppCompatActivity {
    TextView txt_pemesan, txt_ukuran, txt_topping, txt_jumlah, txt_harga;
    public static int harga_size;
    public static int harga_pizza = 25000;
    public static int harga_topping;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        StringBuilder builder = new StringBuilder();

        txt_pemesan = (TextView) findViewById(R.id.txt_pemesan);
        txt_ukuran = (TextView) findViewById(R.id.txt_ukuran);
        txt_topping = (TextView) findViewById(R.id.txt_topping);
        txt_jumlah = (TextView) findViewById(R.id.txt_jumlah);
        txt_harga = (TextView) findViewById(R.id.txt_harga);

        Intent intent = getIntent();
        String nama = intent.getStringExtra("nama");
        String size = intent.getStringExtra("ukuran");
        String topping = intent.getStringExtra("topping");
        int jumlah = intent.getIntExtra("jumlah", 0);
        int harga_topping = intent.getIntExtra("harga_topping", 0);

        if (size.equals("Small")){
            harga_size = 5000;
        }
        else if (size.equals("Medium")){
            harga_size = 8000;
        }
        else if (size.equals("Large")){
            harga_size = 10000;
        }
        int total = (harga_pizza*jumlah)+harga_size+harga_topping;

        txt_pemesan.setText("= "+nama);
        txt_jumlah.setText("= "+jumlah);
        txt_ukuran.setText("= "+size);
        txt_topping.setText("= "+topping);
        txt_harga.setText("= "+Integer.toString(total));
    }
}
