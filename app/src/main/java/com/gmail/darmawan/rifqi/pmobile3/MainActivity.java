package com.gmail.darmawan.rifqi.pmobile3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText nama;
    CheckBox cbDaging, cbJamur, cbKeju, cbSayur, cbTuna;
    RadioButton rb_small, rb_medium, rb_large;
    Button btn_order;
    public static Button btn_plus, btn_min;
    public static TextView tv_jum;
    public static String ukuran;
    public static String topping;
    public static int harga_topping;
    public static String jumlah;
    public static int xjum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nama = (EditText) findViewById(R.id.text_nama);
        cbDaging = (CheckBox) findViewById(R.id.cb_daging);
        cbJamur = (CheckBox) findViewById(R.id.cb_jamur);
        cbKeju = (CheckBox) findViewById(R.id.cb_keju);
        cbSayur = (CheckBox) findViewById(R.id.cb_sayur);
        cbTuna = (CheckBox) findViewById(R.id.cb_tuna);
        rb_small = (RadioButton) findViewById(R.id.rb_small);
        rb_medium = (RadioButton) findViewById(R.id.rb_medium);
        rb_large = (RadioButton) findViewById(R.id.rb_large);
        btn_order = (Button) findViewById(R.id.btn_order);
        btn_plus = (Button) findViewById(R.id.btn_plus);
        btn_min = (Button) findViewById(R.id.btn_min);
        tv_jum = (TextView) findViewById(R.id.tv_jum);
        jumlah = tv_jum.getText().toString();
        xjum = Integer.parseInt(jumlah);
        btn_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                xjum = xjum +1;
                tv_jum.setText(Integer.toString(xjum));
            }
        });
        btn_min.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (xjum <= 0){

                }
                else {
                    xjum = xjum - 1;
                    tv_jum.setText(Integer.toString(xjum));
                }
            }
        });

        btn_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tNama = nama.getText().toString();
                Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                intent.putExtra("nama", tNama);
                intent.putExtra("ukuran", ukuran);
                intent.putExtra("topping", topping);
                intent.putExtra("jumlah", xjum);
                intent.putExtra("harga_topping", harga_topping);
                startActivity(intent);
            }
        });
    }

    public void RbClick(View view) {
        boolean size = ((RadioButton) view).isChecked();
        switch (view.getId()) {
            case R.id.rb_small:
                if (size) {
                    ukuran = "Small";
                }
                break;
            case R.id.rb_medium:
                if (size) {
                    ukuran = "Medium";
                }
                break;
            case R.id.rb_large:
                if (size) {
                    ukuran = "Large";
                }
                break;
        }
    }

    public void CbClick(View view) {
        if (cbDaging.isChecked() && cbJamur.isChecked() && cbKeju.isChecked() && cbSayur.isChecked() && cbTuna.isChecked()) {
            topping = "Daging, Jamur, Keju,\n   Sayur, dan Tuna";
            harga_topping = 15000;
        } else if (cbDaging.isChecked() && cbJamur.isChecked() && cbKeju.isChecked() && cbSayur.isChecked()) {
            topping = "Daging, Jamur, Keju,\n   dan Sayur";
            harga_topping = 12000;
        } else if (cbDaging.isChecked() && cbJamur.isChecked() && cbKeju.isChecked() && cbTuna.isChecked()) {
            topping = "Daging, Jamur, Keju,\n   dan Tuna";
            harga_topping = 12000;
        } else if (cbDaging.isChecked() && cbJamur.isChecked() && cbSayur.isChecked() && cbTuna.isChecked()) {
            topping = "Daging, Jamur, Sayur,\n   dan Tuna";
            harga_topping = 12000;
        } else if (cbDaging.isChecked() && cbKeju.isChecked() && cbSayur.isChecked() && cbTuna.isChecked()) {
            topping = "Daging, Keju, Sayur,\n   dan Tuna";
            harga_topping = 12000;
        } else if (cbJamur.isChecked() && cbKeju.isChecked() && cbSayur.isChecked() && cbTuna.isChecked()) {
            topping = "Jamur, Keju, Sayur,\n   dan Tuna";
            harga_topping = 12000;
        } else if (cbDaging.isChecked() && cbJamur.isChecked() && cbKeju.isChecked()) {
            topping = "Daging, Jamur, dan Keju";
            harga_topping = 9000;
        } else if (cbDaging.isChecked() && cbJamur.isChecked() && cbSayur.isChecked()) {
            topping = "Daging, Jamur, dan Sayur";
            harga_topping = 9000;
        } else if (cbDaging.isChecked() && cbJamur.isChecked() && cbTuna.isChecked()) {
            topping = "Daging, Jamur, dan Tuna";
            harga_topping = 9000;
        } else if (cbDaging.isChecked() && cbKeju.isChecked() && cbSayur.isChecked()) {
            topping = "Daging, Keju, dan Sayur";
            harga_topping = 9000;
        } else if (cbDaging.isChecked() && cbKeju.isChecked() && cbTuna.isChecked()) {
            topping = "Daging, Keju, dan Tuna";
            harga_topping = 9000;
        } else if (cbDaging.isChecked() && cbSayur.isChecked() && cbTuna.isChecked()) {
            topping = "Daging, Sayur, dan Tuna";
            harga_topping = 9000;
        } else if (cbJamur.isChecked() && cbKeju.isChecked() && cbSayur.isChecked()) {
            topping = "Jamur, Keju, dan Sayur";
            harga_topping = 9000;
        } else if (cbJamur.isChecked() && cbSayur.isChecked() && cbTuna.isChecked()) {
            topping = "Jamur, Sayur, dan Tuna";
            harga_topping = 9000;
        } else if (cbKeju.isChecked() && cbSayur.isChecked() && cbTuna.isChecked()) {
            topping = "Keju, Sayur, dan Tuna";
            harga_topping = 9000;
        } else if (cbDaging.isChecked() && cbJamur.isChecked()) {
            topping = "Daging dan Jamur";
            harga_topping = 6000;
        } else if (cbDaging.isChecked() && cbKeju.isChecked()) {
            topping = "Daging dan Keju";
            harga_topping = 6000;
        } else if (cbDaging.isChecked() && cbSayur.isChecked()) {
            topping = "Daging dan Sayur";
            harga_topping = 6000;
        } else if (cbDaging.isChecked() && cbTuna.isChecked()) {
            topping = "Daging dan Tuna";
            harga_topping = 6000;
        } else if (cbJamur.isChecked() && cbKeju.isChecked()) {
            topping = "Jamur dan Keju";
            harga_topping = 6000;
        } else if (cbJamur.isChecked() && cbSayur.isChecked()) {
            topping = "Jamur dan Sayur";
            harga_topping = 6000;
        } else if (cbJamur.isChecked() && cbTuna.isChecked()) {
            topping = "Jamur dan Tuna";
            harga_topping = 6000;
        } else if (cbKeju.isChecked() && cbSayur.isChecked()) {
            topping = "Keju dan Sayur";
            harga_topping = 15000;
        } else if (cbKeju.isChecked() && cbTuna.isChecked()) {
            topping = "Keju dan Tuna";
            harga_topping = 6000;
        } else if (cbSayur.isChecked() && cbTuna.isChecked()) {
            topping = "Sayur dan Tuna";
            harga_topping = 6000;
        }else if (cbDaging.isChecked()) {
            topping = "Daging";
            harga_topping = 3000;
        } else if (cbSayur.isChecked()) {
            topping = "Sayur";
            harga_topping = 3000;
        } else if (cbKeju.isChecked()) {
            topping = "Keju";
            harga_topping = 3000;
        } else if (cbJamur.isChecked()) {
            topping = "Jamur";
            harga_topping = 3000;
        } else if (cbTuna.isChecked()) {
            topping = "Tuna";
            harga_topping = 3000;
        } else if (!cbDaging.isChecked() && !cbJamur.isChecked() && !cbKeju.isChecked() && !cbSayur.isChecked() && !cbTuna.isChecked()){
            topping = "Tidak Ada";
            harga_topping = 0;
        }
    }

}
